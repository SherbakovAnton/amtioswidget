//
//  WidgetViewModel.swift
//  widget
//
//  Created by Mac on 1/31/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import RxSwift

///
class WidgetViewModel : WatchKitDataManagerDelegate  {
    
    fileprivate let log = "WidgetViewModel"
    static let instance = WidgetViewModel()
    
    let observableUserKpiIndicatorList = PublishSubject<Array<KpiIndicator>>()
    let observableUserKpiTest = PublishSubject<Kpi>()
    let observableOnUserLogiIn = PublishSubject<Bool>()
    let observableBrand = PublishSubject<String>()
    
    let firstKpiObserver = PublishSubject<Kpi>()
    let secondKpiObserver = PublishSubject<Kpi>()
    let thirdKpiObserver = PublishSubject<Kpi>()
    
    let bag = DisposeBag()
    
    let userLayer : UserLayer = UserLayer.instanse
    
    private let watchKitDataManager = WatchKitDataManager()
    
    ///**************************************************** METHODS *************************************************
    
    
    // MARK: - WatchKitDataManagerDelegate
    
    private func setupListener() {
        watchKitDataManager.delegate = self
        watchKitDataManager.startListeningForContactUpdates()
    }
    
    func watchKitDataManagerDidUpdateContact(watchKitDataManager: WatchKitDataManager, kpiWidget : KpiForWidget) {
        //setupViewWithContact(it: kpiWidget)
    }
    
     func readContact() {
        print("WIDGET - readContact")
        if let contact = watchKitDataManager.readContact(identifier: "firstKpi") {
            firstKpiObserver.onNext(contact.toKpi())
        }
        if let contact = watchKitDataManager.readContact(identifier: "secondKpi") {
            secondKpiObserver.onNext(contact.toKpi())
        }
        if let contact = watchKitDataManager.readContact(identifier: "thirdKpi") {
            thirdKpiObserver.onNext(contact.toKpi())
        }
    }

    
    private init() {
        readContact()
        setupListener()
        
        userLayer.observableUser
            .observeOn(MainScheduler.instance)
            .debug("WidgetViewModel debug")
            .do(onNext: { print("WidgetViewModel debug \(String(describing: $0))") })
            .subscribe( { (user) in print("user changed - \(user)") } )
            .disposed(by: bag)
        
        userLayer.observableUserKpiList
            .observeOn(MainScheduler.instance)
            .subscribe(onNext : { [weak self] (kpiIndicatorList) in
                kpiIndicatorList.forEach({ (indicator) in
                    //getKpiValueDelegateFunc(kpi: indicator.kpi)
                })
             })
            .disposed(by: bag)
    }
    
    func getUserBrand() {
        observableBrand.onNext(userLayer.getBrand())
    }
    
    func isUserLogIn() {
        //observableOnUserLogiIn.onNext(userLayer.isUserLogIn())
    }
    

    func getUserKpiIndicatorList() {
        if let indicators =  userLayer.getKpiIndicators() {
            observableUserKpiIndicatorList.onNext(indicators)
            indicators.forEach({
                print("\(log) indicator - \($0)")
                //getKpiValueDelegateFunc(kpi: indicator.kpi, observable: observableUserKpiTest)
            })
        }
    }

}


