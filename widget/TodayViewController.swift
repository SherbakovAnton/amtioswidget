//
//  TodayViewController.swift
//  widget
//
//  Created by Mac on 1/30/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import NotificationCenter
import RxSwift
import RxCocoa

class TodayViewController : UIViewController, NCWidgetProviding {
    let bag = DisposeBag()
    fileprivate let log = Mirror(reflecting : self)
    weak var viewModel = WidgetViewModel.instance
    weak var scheduler: Timer?
    
    @IBOutlet weak var firstKpiView : WidgetKpiView!
    @IBOutlet weak var secondKpiView: WidgetKpiView!
    @IBOutlet weak var thirdKpiView: WidgetKpiView!
    //@IBOutlet weak var brandLabel: UILabel!
    
    var kpiViewsArray : Array<WidgetKpiView>? = nil
    
    //************************************************** METHODS ***********************************************
    @IBAction func openApp(_ sender: UIButton) {
        if let appurl = URL(string: "AmtWidegt:") { self.extensionContext!.open(appurl, completionHandler: nil) }
    }
    
    convenience init() {
        self.init(nibName: nil, bundle: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        kpiViewsArray = [firstKpiView, secondKpiView, thirdKpiView]
        firstKpiView.setValueColor(color : ResColor.turquoise)
        secondKpiView.setValueColor(color : ResColor.acidGreen)
        thirdKpiView.setValueColor(color : ResColor.crimson)
        
        viewModel?.observableOnUserLogiIn
            .subscribe( onNext : { [weak self] (logIn) in
                logIn ? self?.setTimer() : self?.onCancelSheduling() })
            .disposed(by: bag)
        
        viewModel?.firstKpiObserver
            .subscribe(onNext : { [weak self] (kpi) in
                print("\(self?.log) first kpi observable react - \(kpi)")
                self?.firstKpiView.bindKpi(kpi: kpi)
            }).disposed(by: bag)
        
        viewModel?.secondKpiObserver
            .subscribe(onNext : { [weak self] (kpi) in
                print("\(self?.log) second kpi observable react - \(kpi)")
                self?.secondKpiView.bindKpi(kpi: kpi)
            }).disposed(by: bag)
        
        viewModel?.thirdKpiObserver
            .subscribe(onNext : { [weak self] (kpi) in
                print("\(self?.log) first kpi observable react - \(kpi)")
                self?.thirdKpiView.bindKpi(kpi: kpi)
            }).disposed(by: bag)
        
        /*viewModel?.observableBrand
            .subscribe( onNext : { [weak self] (brand) in
                self?.brandLabel.text = brand })
            .disposed(by: bag)*/
        
        
        viewModel?.isUserLogIn()
        viewModel?.getUserKpiIndicatorList()
        viewModel?.readContact()
    }
    
    func handleUserKpiIndicators(_ kpiIndicatorList : Array<KpiIndicator>) {
        kpiIndicatorList.forEach { (kpiIndicator) in
            switch kpiIndicator.index {
            //case 1:
                //firstKpiView.bindKpi(kpi: kpiIndicator.kpi)
            case 2:
                secondKpiView.bindKpi(kpi: kpiIndicator.kpi)
            case 3:
                thirdKpiView.bindKpi(kpi: kpiIndicator.kpi)
            default:
                print("\(log) NOTHING")
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        onCancelSheduling()
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        completionHandler(NCUpdateResult.newData)
    }
}

protocol Scheduler {
    /* seconds */
    var scheduleInterval : Int { get }
    
    func setTimer()
    func onSchedule()
    func onCancelSheduling()
}

extension TodayViewController : Scheduler {
    
    var scheduleInterval : Int {
        return 100 // 1 * 60 * 60
    }
    
    func setTimer() {
        scheduler = Timer.scheduledTimer(timeInterval: TimeInterval(scheduleInterval),
                                         target: self,
                                         selector: #selector(TodayViewController.onSchedule),
                                         userInfo: nil,
                                         repeats: true)
    }
    
    func onCancelSheduling() {
        if scheduler != nil {
            scheduler?.invalidate()
            scheduler = nil
        }
    }
    
    @objc func onSchedule() {
        kpiViewsArray?.forEach({ $0.updateValue() })
        print("\(log) - onScheduled")
    }
}
