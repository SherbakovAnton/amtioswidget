//
//  KpiForWidget.swift
//  AmtWidegt
//
//  Created by Mac on 2/15/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation

final class KpiForWidget : NSObject, NSCoding {
    
    private let indexKey = "index"
    private let kpiIdKey = "kpiId"
    private let kpiNameKey = "kpiName"
    private let kpiLastValueKey = "kpiLastValue"
    
    let index : String
    let kpiId : String
    let kpiName : String
    let kpiLastValue : String
    
    init(index : String, kpiId: String, kpiName : String, kpiLastValue : String) {
        self.index = index
        self.kpiId = kpiId
        self.kpiName = kpiName
        self.kpiLastValue = kpiLastValue
    }
    
    // MARK: - NSCoding
    
    required init(coder aDecoder: NSCoder) {
        index = aDecoder.decodeObject(forKey: indexKey) as! String
        kpiId = aDecoder.decodeObject(forKey: kpiIdKey) as! String
        kpiName = aDecoder.decodeObject(forKey: kpiNameKey) as! String
        kpiLastValue = aDecoder.decodeObject(forKey: kpiLastValueKey) as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(index, forKey: indexKey)
        aCoder.encode(kpiId, forKey: kpiIdKey)
        aCoder.encode(kpiName, forKey: kpiNameKey)
        aCoder.encode(kpiLastValue, forKey: kpiLastValueKey)
    }
    
    func toKpi() -> Kpi {
        return Kpi().apply {
            $0.id = Int(kpiId) ?? 0
            $0.name = kpiName
            $0.value = Double(kpiLastValue)
        }
    }
}
