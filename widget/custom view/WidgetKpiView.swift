//
//  WidgetKpiView.swift
//  widget
//
//  Created by Mac on 1/31/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import RxSwift
import MKRingProgressView

class WidgetKpiView : UIView {
    
    private let nibName = "WidgetKpiView"
    
    private var viewModel : WidgetKpiViewViewModel? = nil
    private var kpi : Kpi? = nil
    let bag = DisposeBag()

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var kpiName: UILabel!
    @IBOutlet weak var kpiValue: UILabel!
    //@IBOutlet weak var progressView: UIActivityIndicatorView!
    @IBOutlet weak var updatingPeriodLabel: UILabel!
    
    //var ringProgressView : MKRingProgressView? = nil
    
    public enum ViewSate {
        case updated
        case updating
        case error
        case unauthorized
    }
    
    // *************************************************** METHODS ***************************************************
    
    func setState(state : ViewSate) {
        switch state {
            case .updating:
                //progressView.isHidden = false
                kpiValue.isHidden = false
            case .error:
                //progressView.isHidden = true
                kpiValue.also { $0.isHidden = false; $0.text = "error" }
            case .unauthorized:
                //progressView.isHidden = true
                kpiValue.also { $0.isHidden = false; $0.text = "unauthorized" }
            case .updated:
                //progressView.isHidden = true
                kpiValue.isHidden = false
            }
    }
    
    func bindKpi(kpi : Kpi?) {
        
        print("WidgetKpiView - kpi binded - \(kpi)")
        
        if let letKpi = kpi {
            kpiName.text = letKpi.name ?? "Kpi value not set..."
            if let letValue = letKpi.value { kpiValue.text = String(describing: letValue) }
            
            //updatingState(isUpdatingState: true)
            if !kpiValue.isHidden { setState(state: .updating) }
            
            viewModel = WidgetKpiViewViewModel(kpi: letKpi)
            viewModel?.observableKpi
                .do(onNext: { print("WidgetKpiView onNext \(String(describing: $0.value))") })
                .subscribe(onNext : { [weak self] (kpi) in
                    self?.setState(state: .updated)
                    self?.kpiValue.text = (kpi.value != nil) ? kpi.value?.detectCoeff() : "Not set"
            })
            .disposed(by: bag)
            
            viewModel?.getKpiValue()
        }
    }
    
    func setValueColor(color : UIColor) {
        kpiValue.textColor = color//ResColor.turquoise
    }
    
    func updateValue() {
        viewModel?.getKpiValue()
    }
    
    func clear() {
        kpiName.text = "Kpi name"
        kpiValue.text = "Kpi value"
    }
    
    /// INIT
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        
        print("WidgetKpiView init ")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
        print("WidgetKpiView init ")
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [ .flexibleHeight , .flexibleWidth]
    }
}

/// ViewModel of WidgetKpiView
class WidgetKpiViewViewModel {
    
    fileprivate var kpi : Kpi?
    fileprivate let observableKpi : PublishSubject<Kpi>
    
    required init(kpi : Kpi) {
        self.kpi = kpi
        observableKpi = PublishSubject<Kpi>()
    }
    
    func getKpiValue() {
        getKpiValueDelegateFunc(kpi: kpi, observable: observableKpi)
    }
    
}
