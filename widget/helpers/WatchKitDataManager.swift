//
//  WatchKitDataManager.swift
//  AmtWidegt
//
//  Created by Mac on 2/15/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import MMWormhole

let group = "group.amtwidget"

protocol WatchKitDataManagerDelegate : class {
    func watchKitDataManagerDidUpdateContact(watchKitDataManager: WatchKitDataManager, kpiWidget : KpiForWidget)
}

class WatchKitDataManager : NSObject {
    
    private let kpiWidgetClass = "KpiForWidget"
    
    private let wormhole = MMWormhole(applicationGroupIdentifier : group, optionalDirectory: nil)
    
    weak var delegate : WatchKitDataManagerDelegate?
    
    // MARK: - Public
    
    func sendContact(kpiWidget : KpiForWidget) {
        NSKeyedArchiver.setClassName(kpiWidgetClass, for : KpiForWidget.self)
        wormhole.passMessageObject(kpiWidget, identifier: kpiWidgetClass)
    }
    
    func sendContact(identifier : String, kpiWidget : KpiForWidget) {
        NSKeyedArchiver.setClassName(identifier, for : KpiForWidget.self)
        wormhole.passMessageObject(kpiWidget, identifier: identifier)
    }
    
    func readContact(identifier : String) -> KpiForWidget? {
        NSKeyedUnarchiver.setClass(KpiForWidget.self, forClassName: identifier)
        return wormhole.message(withIdentifier: identifier) as? KpiForWidget
    }
    
    func readContact() -> KpiForWidget? {
        NSKeyedUnarchiver.setClass(KpiForWidget.self, forClassName: kpiWidgetClass)
        return wormhole.message(withIdentifier: kpiWidgetClass) as? KpiForWidget
    }
    
    func startListeningForContactUpdates() {
        wormhole.listenForMessage(withIdentifier: kpiWidgetClass) { [unowned self] (message) in
            if let kpi = message as? KpiForWidget {
                self.delegate?.watchKitDataManagerDidUpdateContact(watchKitDataManager : self, kpiWidget : kpi)
            }
        }
    }
}
