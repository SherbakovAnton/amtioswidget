//
//  MainViewController.swift
//  AmtWidegt
//
//  Created by Mac on 1/25/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var tableKpiContainerView: UIView!
    
    var kpiListViewController : KpiListTableViewController?
    var kpiPanelViewController : KpiPanelViewController?
    
    @IBOutlet weak var gradientBackground: UIImageView!
    var gradientLayer: CAGradientLayer!
    
    private let kpiListId = "kpiList"
    private let kpiPanelId = "kpiPanel"
    
    static func instantiate() -> MainViewController {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
            .instantiateViewController(withIdentifier: "mainController") as! MainViewController
    }
    
    private func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.startPoint = CGPoint(x: CGFloat(0.0), y: CGFloat(0.0))
        gradientLayer.endPoint = CGPoint(x : CGFloat(1.0), y: CGFloat(1.0))
        gradientLayer.colors = [ResColor.gradientFrom.cgColor, ResColor.gradientTo.cgColor]
        gradientBackground.layer.addSublayer(gradientLayer)
    }

    //@IBAction func settingsButtonClick(_ sender: UIBarButtonItem) {
    //    var finishLogin : FinishLoginViewController = FinishLoginViewController.instantiate()
    //    add(finishLogin)
    //}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createGradientLayer()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch (segue.identifier) {
        case kpiListId?:
            kpiListViewController = (segue.destination as? KpiListTableViewController)?.apply { it in
                it.rootView = self.view
                it.containerView = self.tableKpiContainerView
                it.hoverKpiObservable.subscribe( onNext : { self.handleOnKpiPanelHoveredMessage($0) })
            }
        case kpiPanelId?:
            kpiPanelViewController = (segue.destination as? KpiPanelViewController)?.apply{ it in
                 it.rootView = self.view
            }
           
            //kpiListViewController?.hoverKpiObservable.subscribe( onNext : { self.handleOnKpiPanelHoveredMessage($0) })
        default:
            print("Error in [class] MainViewController [method] prepare()")
        }
    }
    
    private func handleOnKpiPanelHoveredMessage(_ hoverMessage : Hover) {
        //print(hoverMessage)
        kpiPanelViewController?.onGetHoverEvent(hoverMessage: hoverMessage)
        
    }

}

struct Hover {
    static public let quntityParts = 3
    var xPart : Int
    var isHovered : Bool
    var kpi : Kpi? = nil
    
    
    init(xPart : Int, isHovered : Bool) {
        self.xPart = xPart
        self.isHovered = isHovered
    }
    
    init(xPart : Int, isHovered : Bool, kpi : Kpi) {
        self.xPart = xPart
        self.isHovered = isHovered
        self.kpi = kpi
    }
}
