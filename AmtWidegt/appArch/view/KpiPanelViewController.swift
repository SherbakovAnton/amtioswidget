//
//  KpiPanelViewController.swift
//  AmtWidegt
//
//  Created by Mac on 1/26/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit

class KpiPanelViewController : UIViewController {

    @IBOutlet weak var firstKpi: KpiView!
    @IBOutlet weak var secondKpi: KpiView!
    @IBOutlet weak var thirdKpi: KpiView!
    
    var allKpiArr : [KpiView?] = []
    var rootView : UIView? = nil
    
    let viewModel : KpiPanelViewModel? = KpiPanelViewModel.instance
    let loadUserKpiViewModel : LoadUserKpiViewModel? = LoadUserKpiViewModel.instance
    
    private let watchKitDataManager = WatchKitDataManager()
    private var hoverAdapter : HoverListViewAdapter? = nil
    
    //************************************************ METHODS **********************************************
    
    
    
    func onGetHoverEvent(hoverMessage : Hover) {
        
        if (hoverMessage.isHovered) {
            onHovered(hoverMessage: hoverMessage)
            hoverAdapter?.hover(viewIndex: hoverMessage.xPart)
        }
        else { onHoverRemoved(hoverMessage: hoverMessage) }
    }
    
    func onHovered(hoverMessage : Hover) {
        switch hoverMessage.xPart {
            case 0:
                firstKpi.hover(isHovered: true)
                if let kpi = hoverMessage.kpi {
                    firstKpi.bindKpi(kpi: kpi)
                    viewModel?.saveFirstKpi(kpi: kpi)
                    sendMessageToWidget(identifier: "firstKpi", index : "0", kpi : kpi)
                }
            case 1:
                secondKpi.hover(isHovered: true)
                if let kpi = hoverMessage.kpi {
                    secondKpi.bindKpi(kpi: kpi)
                    viewModel?.saveSecondKpi(kpi: kpi)
                    sendMessageToWidget(identifier: "secondKpi", index : "1", kpi : kpi)
                }
            case 2:
                thirdKpi.hover(isHovered: true)
                if let kpi = hoverMessage.kpi {
                    thirdKpi.bindKpi(kpi: kpi)
                    viewModel?.saveThirdKpi(kpi: kpi)
                    sendMessageToWidget(identifier: "thirdKpi", index : "2", kpi : kpi)
                }
            default:
                print("ERROR...")
        }
    }
    
    private func sendMessageToWidget(identifier : String, index : String, kpi : Kpi) {
        watchKitDataManager.sendContact(identifier : identifier,
                                        kpiWidget: KpiForWidget(index : index,
                                                                kpiId: String(kpi.id),
                                                                kpiName : kpi.name!,
                                                                kpiLastValue : String(describing: kpi.value) ))
    }
    
    func onHoverRemoved(hoverMessage : Hover) {
        allKpiArr.forEach( { $0?.hover(isHovered: false) })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        allKpiArr = [firstKpi, secondKpi, thirdKpi]
        
        hoverAdapter = HoverListViewAdapter(listView: allKpiArr)
        
        firstKpi.setSequence(KpiSequence.first)
        secondKpi.setSequence(KpiSequence.second)
        thirdKpi.setSequence(KpiSequence.third)
        allKpiArr.forEach { view in view?.backgroundColor = .clear }
        
        loadUserKpiViewModel?.firstUserKpiobservable
            .subscribe(onNext : { self.firstKpi.bindKpi(kpi: $0) })
        
        loadUserKpiViewModel?.secondUserKpiobservable
            .subscribe(onNext : { self.secondKpi.bindKpi(kpi: $0) })
        
        loadUserKpiViewModel?.thirdUserKpiobservable
            .subscribe(onNext : { self.thirdKpi.bindKpi(kpi: $0) })
        
        loadUserKpiViewModel?.loadKpi()
    }
}
