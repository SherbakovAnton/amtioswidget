//
//  FirstLoginViewController.swift
//  AmtWidegt
//
//  Created by Mac on 1/20/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import Dropper
import RxSwift
import RxCocoa
import Toast_Swift
import XCGLogger

public typealias Parameters = [String: Any]

class FirstLoginViewController : UIViewController {
    
    @IBOutlet weak var loginInput : TextView!
    @IBOutlet weak var passwortInput : PasswordTextField!
    @IBOutlet weak var sigButton: UIButton!
    
    private lazy var viewModel : FirstLoginViewModel = FirstLoginViewModel.instance
    private let bag = DisposeBag()
    
    //******************************* METHODS *******************************
    
    /// Static Controller initializer
    static func instantiate() -> FirstLoginViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let viewController = storyboard.instantiateViewController(withIdentifier: "firstLoginViewController") as! FirstLoginViewController
        return viewController
    }
    
    @IBAction func onSignInButtonClicked(_ sender: UIButton) {}

    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.primaryTokenObservble
            .subscribe(onNext : {
                if $0 != nil {
                    log.debug("firstLoginIsSuccessfull")
                    self.remove(self)
                    self.add(FinishLoginViewController.instantiate())
                }
            } )
            .disposed(by: bag)
        
        viewModel.primaryTokenObservble
            .subscribe(onNext: { [weak self] (it) in if it == nil {
                self?.view.makeToast("Creadentials is failed")
                self?.clearAllNotice()
                } })
            .disposed(by: bag)
        
        passwortInput.rx.text
            .subscribe(onNext : { [weak self] (it) in self?.viewModel.userPassword = it })
            .disposed(by: bag)
        
        loginInput.rx.text
            .subscribe(onNext : { [weak self] (it) in self?.viewModel.userLogin = it })
            .disposed(by: bag)
        
        sigButton.rx.tap
            .subscribe(onNext : { [weak self] (it) in
                let login = self?.loginInput.text ?? ""
                let password = self?.passwortInput.text ?? ""
                if !login.isEmpty && !password.isEmpty {
                    self?.pleaseWait()
                    self?.viewModel.getPrimaryToken([ "login": login, "password": password ])
                } else {
                    self?.view.makeToast("Please set login and password")
                }
            }).disposed(by: bag)
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewModel.userIsLoginObservable
            .subscribe(onNext : {
                if $0 {
                    //self.remove(self)
                    
                    let nvc = UIStoryboard(name: "Main", bundle: Bundle.main)
                        .instantiateViewController(withIdentifier: "mainvc") as! UINavigationController
                    
                    self.present(nvc, animated: true)
                }
            })
            .disposed(by: bag)
        
        viewModel.isUserLogin()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.clearAllNotice()
    }
}
