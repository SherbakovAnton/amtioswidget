//
//  KpiListTableViewController.swift
//  AmtWidegt
//
//  Created by Mac on 1/25/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import XCGLogger
import RxSwift
import DynamicColor

class KpiListTableViewController : UITableViewController {
    
    var rootView : UIView? = nil
    var containerView : UIView? = nil
    
    @IBOutlet var kpiTableView : UITableView!
    
    var modelArray = [Kpi]() {
        didSet {
            if (kpiTableView != nil) {
                kpiTableView.reloadData()
            }
        }
    }
    
    let viewModel = KpiListViewModel.instance
    
    // Observables
    let hoverKpiObservable = PublishSubject<Hover>()
    let selectedKpiObservable = PublishSubject<Hover>()
    
    //**************************************************** METHODS ****************************************************

    override func viewDidLoad() {
        super.viewDidLoad()
        
        gradient = DynamicGradient(colors: [ResColor.listGradientFrom, ResColor.listGradientTo])
        
        viewModel.kpiObservable
            .subscribe(onNext : {
                $0.forEach( { kpi in self.modelArray.append(kpi) })
            })
        
        viewModel.getKpiList()
        
        let longpress = UILongPressGestureRecognizer(target: self, action: #selector(KpiListTableViewController.longPressGestureRecognized))
        tableView.addGestureRecognizer(longpress)
    }
    
    @objc func longPressGestureRecognized(gestureRecognizer: UIGestureRecognizer) {
        let longPress = gestureRecognizer as! UILongPressGestureRecognizer
        let state = longPress.state
        let locationInView = longPress.location(in: tableView)
        let locationInRootView = longPress.location(in: rootView)
        let locationInContainer = longPress.location(in: containerView)
        let indexPath = tableView.indexPathForRow(at: locationInView)
        
        struct My {
            static var cellSnapshot : UIView? = nil
            static var cellIsAnimating : Bool = false
            static var cellNeedToShow : Bool = false
        }
        struct Path {
            static var initialIndexPath : NSIndexPath? = nil
        }
        
        func handleDragging() {
            let screenWidth = ScreenHelper.width
            let xPart =  Int(locationInRootView.x) / (Int(screenWidth) / Hover.quntityParts)
            
            log.debug("location Y - \(locationInContainer.y)")
            
            hoverKpiObservable.onNext( Hover(xPart: xPart, isHovered: locationInContainer.y < 0) )
        }
        
        func handleDragging(_ kpi : Kpi) {
            let screenWidth = ScreenHelper.width
            let xPart =  Int(locationInRootView.x) / (Int(screenWidth) / Hover.quntityParts)
            hoverKpiObservable.onNext( Hover(xPart: xPart, isHovered: locationInContainer.y < 0, kpi: kpi) )
        }
        
        switch state {
        case UIGestureRecognizerState.began:
            if indexPath != nil {
                Path.initialIndexPath = indexPath! as NSIndexPath
                let cell = tableView.cellForRow(at: indexPath!) as! ListKpiView!
                
                let labelRect = ScreenHelper.width / 3
                let label = DragAndDropView(frame: CGRect(x: 0, y: 0, width: labelRect, height: labelRect))
                label.center = CGPoint(x: 160, y: 284)
                label.setText(text: (cell?.kpiNameLabel.text)!)
                My.cellSnapshot  = label
                
                var center = cell?.center
                My.cellSnapshot!.center = center!
                My.cellSnapshot!.alpha = 0.0
                rootView?.addSubview(My.cellSnapshot!)
                
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    center?.y = locationInRootView.y - 50
                    center?.x = locationInRootView.x
                    My.cellIsAnimating = true
                    My.cellSnapshot!.center = center!
                    My.cellSnapshot!.transform = CGAffineTransform(scaleX: 1.05, y: 1.05)
                    My.cellSnapshot!.alpha = 0.98
                }, completion: { (finished) -> Void in
                    if finished {
                        My.cellIsAnimating = false
                        if My.cellNeedToShow {
                            My.cellNeedToShow = false
                            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                                cell?.alpha = 1
                            })
                        }
                    }
                })
            }
            
        case UIGestureRecognizerState.changed:
            if My.cellSnapshot != nil {
                var center = My.cellSnapshot!.center
                //print("X - \(locationInView.x)       Y - \(locationInView.y)")
                print(ScreenHelper.width)
                center.y = locationInRootView.y - 50
                center.x = locationInRootView.x
                My.cellSnapshot!.center = center
                
                handleDragging()
                
                
            }
        default:
            if Path.initialIndexPath != nil {
                let cell = tableView.cellForRow(at: Path.initialIndexPath! as IndexPath) as! ListKpiView!
                //print("ended - \(String(describing: Path.initialIndexPath)) - \(cell?.kpiNameLabel) - \(cell?.kpiEntity?.name)")
                if let kpi = cell?.kpiEntity {
                    handleDragging(kpi)
                }
                My.cellSnapshot!.alpha = 0.0
            }
        }
    }
    
    func snapshotOfCell(inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        inputView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext() as! UIImage
        UIGraphicsEndImageContext()
        
        let cellSnapshot : UIView = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        //cellSnapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
    
    private func handleDragAndDrop() {
    
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = modelArray.count
        palette = gradient?.colorPalette(amount: UInt(count))
        return count
    }
    
    var gradient : DynamicGradient? = nil
    var palette : [DynamicColor]? = nil
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .none
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListKpiViewCell", for: indexPath) as! ListKpiView
        cell.bind(kpiEntity: modelArray[indexPath.row])
        cell.gradientIndicator.backgroundColor = palette?[indexPath.row]
        return cell
    }

}
