//
//  FinishLoginViewController.swift
//  AmtWidegt
//
//  Created by Mac on 1/20/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import Dropper
import RxSwift
import RxCocoa
import Toast_Swift
import RealmSwift
import XCGLogger

class FinishLoginViewController : UIViewController {
    
    /*@IBOutlet weak var companySelectionButton : UIButton!
    @IBOutlet weak var periodSelectionButton : UIButton!
    @IBOutlet weak var brandSelectionButton : UIButton!*/
    @IBOutlet weak var conapany: ButtonView!
    @IBOutlet weak var period: ButtonView!
    @IBOutlet weak var brand: ButtonView!
    
    @IBOutlet weak var logoutButton: UIButton!
    
    private lazy var companyDroper  = Dropper(width : 200, height : 400)
    private lazy var periodDroper   = Dropper(width : 200, height : 400)
    private lazy var brandDroper    = Dropper(width : 200, height : 400)
    
    private lazy var viewModel = FinishLoginViewModel.instance
    private lazy var loginViewModel = LoginStateViewModel.instance
    private lazy var logoutViewModel = LogoutViewModel.instance
    
    private let bag = DisposeBag()
    
    //**************************************************** METHODS ****************************************************
    
    @IBAction func onSigIn(_ sender: UIButton) {
        viewModel.sigIn()
        /*let nvc = UIStoryboard(name: "Main", bundle: Bundle.main)
            .instantiateViewController(withIdentifier: "mainvc") as! UINavigationController
        self.present(nvc, animated: true)*/
    }
    
    static func instantiate() -> FinishLoginViewController {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
                .instantiateViewController(withIdentifier: "finishLogin") as! FinishLoginViewController
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        conapany.onTitleChanged = { companyName in
            self.viewModel.selectedCompany = companyName
            self.viewModel.selectedBrand = nil
            self.brand.text = "Brand"
        }
        period.onTitleChanged   = { period in self.viewModel.selectedPeriod = period }
        brand.onTitleChanged    = { brand in self.viewModel.selectedBrand = brand }
        
        conapany.rx.tap
            .subscribe( onNext : {
                self.viewModel.getCompanies()
                self.conapany.setProgress(true) })
        .disposed(by: bag)
        
        period.rx.tap
            .subscribe( onNext : {
                self.periodDroper.setItems(items: ["Year", "Month", "Weak", "Day"])
                    .touchDropper(self.period) })
            .disposed(by: bag)
        
        brand.rx.tap
            .subscribe( onNext : {
                self.viewModel.getBrands()
                self.brand.setProgress(true)
            })
            .disposed(by: bag)
        
        viewModel.companyObservable.subscribe(
            onNext : {
                self.companyDroper.setItems(companies: $0).touchDropper(self.conapany)
                self.conapany.setProgress(false) })
            .disposed(by: bag)
        
        viewModel.brandObservable.subscribe(
            onNext : {
                self.brandDroper.setItems(brands: $0)
                    .touchDropper(self.brand)
                self.brand.setProgress(false) })
            .disposed(by: bag)
        
        viewModel.loginSuccessfullObservable
            .subscribe(onNext : {
                if $0 {
                    let nvc = UIStoryboard(name: "Main", bundle: Bundle.main)
                        .instantiateViewController(withIdentifier: "mainvc") as! UINavigationController
                    self.present(nvc, animated: true)
                }
            } )
            .disposed(by: bag)
        
        viewModel.errorSignInObservable.subscribe( onNext : { self.view.makeToast($0) })
            .disposed(by: bag)
        
        ifIsLogin()
    }
    
    private func ifIsLogin() {
        
        logoutViewModel.logOutObservable.subscribe(onNext : {
            if $0 {
                self.clearAllNotice()
                let nvc = UIStoryboard(name: "Main", bundle: Bundle.main)
                    .instantiateInitialViewController()
                
                self.present(nvc!, animated: true)
            }
        }).disposed(by: bag)
        
        logoutButton.rx.tap
            .subscribe( onNext : {
                log.debug("logoutButton tapped")
                self.pleaseWait()
                self.logoutViewModel.logout()
            }).disposed(by: bag)
        
        loginViewModel.loginObservable.subscribe(onNext : {
            self.logoutButton.isHidden = !$0
        }).disposed(by: bag)
        
        loginViewModel.isLogin()
    }
}






