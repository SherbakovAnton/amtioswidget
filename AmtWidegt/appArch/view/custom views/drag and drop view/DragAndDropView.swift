//
//  DragAndDropView.swift
//  AmtWidegt
//
//  Created by Admin on 12.02.2018.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit

class DragAndDropView : UIView {
    
    private let nibName = "DragAndDropView"
    private var cornerRadius : CGFloat = CGFloat(8)
    
    @IBOutlet weak var contentView: UIView!

    @IBOutlet weak var kpiName: UILabel!
    
    func setText(text : String) {
        kpiName.text = text
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(contentView)
    
        //contentView.frame = self.bounds
        //contentView.autoresizingMask = [ .flexibleHeight , .flexibleWidth]
        contentView.setRadius(radius: cornerRadius)
        
        
    }
    
}
