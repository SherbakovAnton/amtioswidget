//
//  ListKpiView.swift
//  AmtWidegt
//
//  Created by Mac on 1/25/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import RxSwift

class ListKpiView : UITableViewCell {
    
    private let bag = DisposeBag()

    var kpiEntity : Kpi?
    let kpiValueObservable = PublishSubject<String>()
    
    private let viewModel = KpiValueViewModel.instance
    
    @IBOutlet weak var kpiValueLabel: UILabel!
    @IBOutlet weak var kpiNameLabel: UILabel!
    @IBOutlet weak var gradientIndicator: UIView!
    @IBOutlet weak var progressIndicator: UIActivityIndicatorView!
    
    var kpi : Kpi?
    let observableKpi = PublishSubject<Kpi>()
    
    var updatedKpiValue = false
    
    func bind(kpiEntity : Kpi) {
        self.kpiEntity = kpiEntity
        kpiNameLabel.text = kpiEntity.name
        if !updatedKpiValue {
            progressIndicator.startAnimating()
            getKpiValueDelegateFunc(kpi : kpiEntity, observable : observableKpi) }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        observableKpi
            .subscribe(onNext : {
                self.kpiValueLabel.text = $0.value == nil ? "N/A" : $0.value?.detectCoeff()
                self.updatedKpiValue = true
                self.progressIndicator.stopAnimating()
            })
            .disposed(by: bag)
    }
    
    override func draw(_ rect: CGRect) {
        //if progressIndicator.isAnimating { progressIndicator.stopAnimating() }
    }
}
