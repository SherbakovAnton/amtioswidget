//
//  HoverListViewAdapter.swift
//  AmtWidegt
//
//  Created by Mac on 2/16/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation

class HoverListViewAdapter {
    
    let listView : [HoveredUIView?]
    
    required init(listView : [HoveredUIView?]) {
        self.listView = listView
    }
    
    func hover(viewIndex : Int) {
        for (index, element) in listView.enumerated() {
            if viewIndex == index { element?.hover(isHovered: true) }
            else { element?.hover(isHovered: false) }
        }
    }
    
}
