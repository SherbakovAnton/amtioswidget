//
//  KpiView.swift
//  AmtWidegt
//
//  Created by Mac on 1/26/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import RxSwift
import XCGLogger

protocol HoveredUIView {
    func hover(isHovered : Bool)
}

open class KpiView: UIView, HoveredUIView {
    
    private let nibName = "KpiView"
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var kpiName: UILabel!
    @IBOutlet weak var kpiValue: UILabel!
    @IBOutlet weak var backgroundCircleImage: UIImageView!
    
    var kpi : Kpi?
    let observableKpi = PublishSubject<Kpi>()
    
    private let viewModel = KpiValueViewModel.instance
    
    private var isHovered = false
    
    func bindKpi(kpi : Kpi) {
        log.debug("BINDED - \(kpi)")
        self.kpi = kpi
        kpiName.text = kpi.name
        
        if kpi.value != nil {
            observableKpi.onNext(kpi)
        } else {
            getKpiValueDelegateFunc(kpi : kpi, observable : observableKpi)
        }
        
        
        //viewModel.getKpiValue(kpi , kpiValueObservable)
        
    }
    
    func hover(isHovered : Bool) {
        
        if self.isHovered == isHovered { return }
        else {
            if isHovered { reactOnChangeHover(isHover : isHovered) }
            self.isHovered = isHovered
        }
        
        /*if (isHovered) {
            log.debug("isHovered")
        } else {
            log.debug("not Hovered")
        }*/
    }
    
    private func reactOnChangeHover(isHover : Bool) {
        
        let scaleStartValue = isHover ? CGFloat(1.2) : CGFloat(0.8)
        let duration = 0.25
        
        UIView.animate(withDuration: duration, delay: 0.0, options: [.transitionCurlDown],
                       animations: { self.backgroundCircleImage.transform = CGAffineTransform(scaleX: scaleStartValue, y: scaleStartValue) },
                       completion: { _ in UIView.animate(withDuration: duration) {
                            self.backgroundCircleImage.transform = CGAffineTransform.identity
                        }
        })
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [ .flexibleHeight , .flexibleWidth]
        
        observableKpi
            .subscribe(onNext : {
                if let value = $0.value { self.kpiValue.text = value.detectCoeff() }
            })
            //.disposed(by: bag)
       
    }
}
