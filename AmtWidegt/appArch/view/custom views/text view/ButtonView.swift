//
//  ButtonView.swift
//  AmtWidegt
//
//  Created by Mac on 2/6/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import UIKit
import Dropper

@IBDesignable open class ButtonView : UIButton, DropperDelegate {
    
    private var rightImageView : UIImageView? = nil
    private var rightActivityIndicator : UIActivityIndicatorView? = nil
    
    private lazy var selfWidth = self.frame.width//self.view.frame.width
    private lazy var requiredViewWidth = self.frame.size.height
    private lazy var halfWidth = requiredViewWidth / 2
    private lazy var indents = (requiredViewWidth - halfWidth) / 2
    
    var onTitleChanged : ((String) -> Void)? = nil
    
   
    
    
    
    /** Default initializer for the View */
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    /** Default initializer for the View done from storyboard */
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    @IBInspectable var text : String? = nil {
        didSet {
            self.setTitle(text, for: .normal)
        }
    }
    
    @IBInspectable var textColor: UIColor = UIColor.clear {
        didSet {
            self.setTitleColor(textColor, for: .normal)
        }
    }
    
    /// Right Image
    @IBInspectable var rightImageResource : UIImage? = nil {
        didSet {
            rightImageView = UIImageView(frame: CGRect(x: halfWidth / 2 , y: halfWidth / 2, width: halfWidth * 0.75, height: halfWidth * 0.75)).apply { it  in
                it.image = rightImageResource
                
                UIView(frame: CGRect(x: selfWidth - requiredViewWidth , y: 0, width: requiredViewWidth, height: requiredViewWidth)).also {
                    
                    $0.contentMode = .scaleAspectFit
                    $0.addSubview(it)
                    it.center = CGPoint(x: $0.frame.width / 2, y: $0.frame.width / 2)
                    //$0.backgroundColor = .red
                    self.addSubview($0)
                }
            }
        }
    }
    
    /// Left Image
    @IBInspectable var leftImageResource : UIImage? = nil {
        didSet {
            
            UIImageView(frame: CGRect(x: indents, y: indents, width: halfWidth, height: halfWidth)).also { it  in
                
                it.image = leftImageResource
                
                UIView().also {
                    $0.contentMode = .scaleAspectFit
                    $0.frame = CGRect(x: 0, y: 0, width: requiredViewWidth, height: requiredViewWidth)
                    $0.addSubview(it)
                    self.addSubview($0)
                    //padding += self.frame.size.height
                }
            }
        }
    }
    
    /********************* Private methods *********************/
    
    /** Custom initializer for the TextView */
    private func setup() {
        
    }
    
    open func setProgress(_ enable : Bool) {
        if enable {
            rightImageView?.isHidden = true
            if let progress = rightActivityIndicator {
                progress.isHidden = false
            } else {
                rightActivityIndicator = addRightActivityIndicator()
            }
        } else {
            rightImageView?.isHidden = false
            rightActivityIndicator?.isHidden = true
        }
    }
    
    public func addRightActivityIndicator() -> UIActivityIndicatorView {
        return UIActivityIndicatorView(frame: CGRect(x: 0 , y: 0, width: requiredViewWidth, height: requiredViewWidth)).apply { indicator in
            UIView().also { container in
                
                container.contentMode = .scaleAspectFit
                container.frame = getRightCorner()
                container.addSubview(indicator)
                self.addSubview(container)
                //container.backgroundColor = .red
            }
            indicator.startAnimating()
        }
    }
    
    func getRightCorner() -> CGRect {
        print("ext view - requiredViewWidth - \(requiredViewWidth)")
        return CGRect(x: self.frame.size.width - requiredViewWidth, y: 0, width: requiredViewWidth, height: requiredViewWidth)
    }
    
    public func DropperSelectedRow(_ path: IndexPath, contents: String) {
        if let listener = onTitleChanged { listener(contents) }
        self.setTitle(contents, for: .normal)
    }
    
    
    
}
