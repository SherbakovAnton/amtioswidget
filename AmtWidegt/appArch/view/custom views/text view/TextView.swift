//
//  TextView.swift
//  AmtWidegt
//
//  Created by Mac on 2/6/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit

@IBDesignable open class TextView : UITextField {
    
    private var  privatePadding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    /********************* Override methods *********************/
    
    
    /** Default initializer for the TextView */
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    /** Default initializer for the TextView done from storyboard */
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, privatePadding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, privatePadding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, privatePadding)
    }
    
    /********************* InterfaceBuilder methods *********************/
    
    /// Hint text
    @IBInspectable dynamic open var hint : String = "" {
        didSet {
            self.placeholder = hint
        }
    }
    
    /// Corner radius
    @IBInspectable dynamic open var cornerRadius : CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    /// Border width
    @IBInspectable  dynamic open var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    /// Internal padding width (default value = 10)
    @IBInspectable var padding : CGFloat = 10 {
        didSet {
            privatePadding = UIEdgeInsets(top: 0, left: padding, bottom: 0, right: padding)
        }
    }
    
    /// Left Image
    @IBInspectable var leftImageResource : UIImage? = nil {
        didSet {
            updateView(leftImageResource)
        }
    }
    
    /********************* Private methods *********************/
    
    /** Custom initializer for the TextView */
    func setup() {
        self.autocapitalizationType = .none
        self.autocorrectionType = .no
        self.keyboardType = .asciiCapable
    }
    
    func updateView(_ imageRes : UIImage?) {
        if let image = imageRes {
            
            let requiredViewWidth = self.frame.size.height
            let halfWidth = requiredViewWidth / 2
            let indents = (requiredViewWidth - halfWidth) / 2
    
            UIImageView(frame: CGRect(x: indents, y: indents, width: halfWidth, height: halfWidth)).also { it  in
                
                leftViewMode = UITextFieldViewMode.always
                it.image = image
                
                leftView = UIView().apply {
                    $0.contentMode = .scaleAspectFit
                    $0.frame = CGRect(x: 0, y: 0, width: requiredViewWidth, height: requiredViewWidth)
                    $0.addSubview(it)
                    padding += self.frame.size.height
                }
            }
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            //imageView.tintColor = color
        } else {
            leftViewMode = UITextFieldViewMode.never
            leftView = nil
        }
    }
}
