//
//  Ruleable.swift
//  AmtWidegt
//
//  Created by Mac on 2/5/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation

/**
 *  Protocol that represent something that conforms to a Rule
 */
public protocol Ruleable {
    
    func validate(_ value: String) -> Bool
    
    func errorMessage() -> String
    
}
