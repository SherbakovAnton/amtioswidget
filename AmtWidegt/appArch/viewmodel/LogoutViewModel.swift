//
//  LogoutViewModel.swift
//  AmtWidegt
//
//  Created by Mac on 2/19/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import RxSwift


class LogoutViewModel {
    
    let logOutObservable = PublishSubject<Bool>()
    
    func logout() {
        UserLayer.instanse.logout(callback: logOutObservable)
    }
    
    private init() {}
    static let instance = LogoutViewModel()
}
