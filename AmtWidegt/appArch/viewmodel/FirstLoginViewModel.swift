//
//  FirstLoginViewModel.swift
//  AmtWidegt
//
//  Created by Mac on 1/24/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import RxSwift
import XCGLogger

class FirstLoginViewModel {
    
    let primaryTokenObservble   = PublishSubject<String?>()
    let userIsLoginObservable   = PublishSubject<Bool>()
    
    
    private let user_ : User = User()
    private let userLayer = UserLayer.instanse
    private let bag = DisposeBag()
    
    var userLogin : String? = nil {
        didSet { log.debug(userLogin) }
    }
    
    var userPassword : String? = nil {
        didSet { log.debug(userPassword) }
    }

    func getPrimaryToken(_ parameters : Parameters ) {
        WebClient.webClientInstance.getPrimaryToken(parameters, primaryTokenObservble)
    }
    
    func isUserLogin() { userLayer.isUserLogIn(callback : userIsLoginObservable) }
    
    private init() {
        primaryTokenObservble
            .do(onNext: { log.debug("primaryTokenObservble \($0)") })
            .subscribe(onNext : {
                log.debug("primaryTokenObservble \($0)")
                if let primaryToken = $0 {
                    let tempUser = User()
                    
                    tempUser.login = self.userLogin
                    tempUser.password = self.userPassword
                    tempUser.primaryToken = primaryToken
                    self.userLayer.saveUser(user: tempUser)
                }
            } )
            .disposed(by: bag)
    }
    static let instance = FirstLoginViewModel()
}
