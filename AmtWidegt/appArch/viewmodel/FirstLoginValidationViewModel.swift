
//
//  FirstLoginValidationViewModel.swift
//  AmtWidegt
//
//  Created by Admin on 13.02.2018.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import RxSwift

class FirstLoginValidationViewModel : Validator {
    
    var isLoginEmpty = true
    var isPasswordEmpty = true
    
    let loginValiddationObservable = PublishSubject<ValidationResult>()
    
    func checkValidation() {
        if !isLoginEmpty {
            loginValiddationObservable.onNext(ValidationResult(errorMessage: StringRes.loginIsNotValid, isValid: false))
        } else if !isPasswordEmpty {
            loginValiddationObservable.onNext(ValidationResult(errorMessage: StringRes.passwordIsNotValid, isValid: false))
        }
    }
    
    static let instance = FirstLoginValidationViewModel()
    private init() {
        
    }
    
}
