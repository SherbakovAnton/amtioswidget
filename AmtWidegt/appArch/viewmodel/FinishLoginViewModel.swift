//
//  FinishLoginViewModel.swift
//  AmtWidegt
//
//  Created by Mac on 1/24/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import RxSwift
import RealmSwift

class FinishLoginViewModel {
    
    let companyObservable = PublishSubject<Array<Company>>()
    let brandObservable = PublishSubject<Array<Brand>>()
    let loginSuccessfullObservable = PublishSubject<Bool>()
    
    private let userLayer = UserLayer.instanse
    
    private var companies = Array<Company>()
    private var brands = Array<Brand>()
    
    let errorSignInObservable = PublishSubject<String>()
    
    var selectedCompany : String?
    var selectedPeriod : String?
    var selectedBrand : String?
    
    private let bag = DisposeBag()
    
    
    
    func getCompanies() {
        if let token = userLayer.getUser()?.primaryToken {
            WebClient.webClientInstance.getCompanies(primaryToken : token, companyObservable)
        }
    }
    
    private func getSelectedCompany(name : String) -> Company? {
        return companies
            .filter { name == $0.name }
            .first
    }
    
    func getBrands() {
        userLayer.getUser()?.updateEntity { (user) in
            let company = self.getSelectedCompany(name: self.selectedCompany ?? "nil")
            user.company = company
            user.secondaryToken = company?.id
        }
        
        if let generalToken = userLayer.getUser()?.generalToken() {
             WebClient.webClientInstance.getBrands(generalToken : generalToken, brandObservable)
        }
    }
    
    private func getSelectedBrand(name : String) -> Brand? {
        return brands
            .filter { name == $0.name }
            .first
    }
    
    func sigIn() {
        if selectedCompany == nil { errorSignInObservable.onNext("Company is not selected"); return }
        if selectedPeriod  == nil { errorSignInObservable.onNext("Period is not selected"); return  }
        if selectedBrand   == nil { errorSignInObservable.onNext("Brand is not selected"); return  }
        loginSuccessfullObservable.onNext(true)
    }
    
    private init() {
        companyObservable
            .subscribe(onNext : { self.companies = $0 })
            .disposed(by: bag)
        
        brandObservable
            .subscribe(onNext : { self.brands = $0 })
            .disposed(by: bag)
        
        
        loginSuccessfullObservable
            .subscribe(onNext : {
                print("FinishLoginViewModel - loginSuccessfullObservable - \($0)")
                self.userLayer.getUser()?.updateEntity { (user) in
                    user.company = self.getSelectedCompany(name: self.selectedCompany ?? "nil")
                    user.brand = self.getSelectedBrand(name: self.selectedBrand ?? "nil")
                    user.period = self.selectedPeriod }
                self.userLayer.getUser()
                })
            .disposed(by : bag)
    }
    
    static let instance = FinishLoginViewModel()
    
}
