//
//  LoginStateViewModel.swift
//  AmtWidegt
//
//  Created by Mac on 2/19/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import RxSwift

class LoginStateViewModel {
    
    let loginObservable = PublishSubject<Bool>()
    
    func isLogin() {
        UserLayer.instanse.isUserLogIn(callback: loginObservable)
    }
    
    private init() {}
    static let instance = LoginStateViewModel()
}
