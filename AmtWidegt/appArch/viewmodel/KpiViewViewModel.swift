//
//  KpiViewViewModel.swift
//  AmtWidegt
//
//  Created by Mac on 2/7/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import RxSwift

class KpiViewViewModel {
    
    fileprivate let log = "KpiViewViewModel"
    fileprivate var kpi : Kpi? = nil
    
    let kpiValueObservable = PublishSubject<Kpi>()
    let bag = DisposeBag()
    
    func bindKpi(kpi : Kpi) {
        self.kpi = kpi
        
    }
}
