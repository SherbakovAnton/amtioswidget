//
//  KpiPanelViewModel.swift
//  AmtWidegt
//
//  Created by Mac on 1/29/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

class KpiPanelViewModel {
    
    private let userLayer = UserLayer.instanse
    private let bag = DisposeBag()
    
    private var listHolder : [KpiIndicator?] = [nil, nil, nil]
    
    
    func saveFirstKpi(kpi : Kpi) {
        let realm = try! Realm()
        try! realm.write {
            realm.create(User.self, value: ["id": userLayer.getUserId(), "firstKpi": kpi], update: true)
        }
    }
    func saveSecondKpi(kpi : Kpi) {
        let realm = try! Realm()
        try! realm.write {
            realm.create(User.self, value: ["id": userLayer.getUserId(), "secondKpi": kpi], update: true)
        }
    }
    func saveThirdKpi(kpi : Kpi) {
        let realm = try! Realm()
        try! realm.write {
            realm.create(User.self, value: ["id": userLayer.getUserId(), "thirdKpi": kpi], update: true)
        }
        userLayer.getUser()
    }
    
    private init() {}
    static let instance = KpiPanelViewModel()
    
}
