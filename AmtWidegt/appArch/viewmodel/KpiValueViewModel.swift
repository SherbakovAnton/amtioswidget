//
//  KpiValueViewModel.swift
//  AmtWidegt
//
//  Created by Mac on 1/29/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import RxSwift

class KpiValueViewModel {
    
    func getKpiValue(_ kpi : Kpi, _ observable : PublishSubject<Kpi>) {
        
        let userLayer = UserLayer.instanse
        let a = userLayer.getUser()?.period
        
        let period = getInterval(Period.year)
        
        WebClient.webClientInstance.getKpiValue(GetKpiRequest().apply {
            $0.keyPerformanceIndicatorIds = [String(kpi.id)]
            $0.brandId = "b0000000-0000-0000-0000-000000000002"
            $0.fromDate = period.0
            $0.toDate = period.1
        }, observable)
        
    }
    
    private init() {}
    static let instance = KpiValueViewModel()
    
}
