//
//  Validator.swift
//  AmtWidegt
//
//  Created by Admin on 13.02.2018.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation

protocol Validator {
    func checkValidation()
}

struct ValidationResult {
    let errorMessage : String
    let isValid : Bool
}
