//
//  KpiListViewModel.swift
//  AmtWidegt
//
//  Created by Mac on 1/26/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import RxSwift

class KpiListViewModel {
    
    private init() {}
    static let instance = KpiListViewModel()
    
    private let userLayer = UserLayer.instanse
    
    let kpiObservable = PublishSubject<Array<Kpi>>()
    
    func getKpiList() {
        if let token = userLayer.getUser()?.generalToken() {
            WebClient.webClientInstance.getKpiList(generalToken: token, kpiObservable)
        }
    }
    
}
