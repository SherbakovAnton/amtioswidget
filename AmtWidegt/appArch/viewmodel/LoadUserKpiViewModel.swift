//
//  LoadUserKpiViewModel.swift
//  AmtWidegt
//
//  Created by Mac on 2/15/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

class LoadUserKpiViewModel {
    
    let firstUserKpiobservable      = PublishSubject<Kpi>()
    let secondUserKpiobservable     = PublishSubject<Kpi>()
    let thirdUserKpiobservable      = PublishSubject<Kpi>()
    
    private let userLayer = UserLayer.instanse
    private let bag = DisposeBag()
    
    func loadKpi() { loadFirst() ; loadSecond() ; loadThird() }
    
    private func loadFirst() {
        if let kpi = userLayer.getUser()?.firstKpi { firstUserKpiobservable.onNext(kpi) }
    }
    
    private func loadSecond() {
        if let kpi = userLayer.getUser()?.secondKpi { secondUserKpiobservable.onNext(kpi) }
    }
    
    private func loadThird() {
        if let kpi = userLayer.getUser()?.thirdKpi { thirdUserKpiobservable.onNext(kpi) }
    }
    
    private init() {}
    static let instance = LoadUserKpiViewModel()
    
}
