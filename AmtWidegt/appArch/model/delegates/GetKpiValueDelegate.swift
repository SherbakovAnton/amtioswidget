//
//  GetKpiValueDelegate.swift
//  AmtWidegt
//
//  Created by Mac on 2/1/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import RxSwift
import XCGLogger

func getKpiValueDelegateFunc(kpi: Kpi?, observable: PublishSubject<Kpi>) {
    
    let userLayer = UserLayer.instanse
    let a = userLayer.getUser()?.period
    
    print("getKpiValueDelegateFunc \(userLayer.getUser()?.period)")
    
    var period : (String, String)? = nil
    switch userLayer.getUser()?.period {
    case "Year"?:
        period = getInterval(Period.year)
    case "Month"?:
        period = getInterval(Period.month)
    case "Weak"?:
        period = getInterval(Period.week)
    case "Day"?:
        period = getInterval(Period.day)
    default:
        period = getInterval(Period.year)
    }
    
    WebClient.webClientInstance.getKpiValue(GetKpiRequest().apply { it in
        if let kpiId_ = kpi?.id {
            it.keyPerformanceIndicatorIds = [String(kpiId_)]
            it.brandId = userLayer.getUser()?.brand?.id
            it.fromDate = period?.1
            it.toDate = period?.0
        }
    }, observable)
}
