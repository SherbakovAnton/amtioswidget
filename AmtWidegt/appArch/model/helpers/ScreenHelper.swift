//
//  ScreenHelper.swift
//  AmtWidegt
//
//  Created by Mac on 1/28/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import UIKit

class ScreenHelper {
    private static let screenSize = UIScreen.main.bounds
    static let width = screenSize.width
    static let height = screenSize.height
}

