//
//  KpiSequenceManager.swift
//  AmtWidegt
//
//  Created by Admin on 12.02.2018.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import UIKit

enum KpiSequence {
    case first
    case second
    case third
}

extension KpiView {
    
    func setSequence(_ sequence : KpiSequence) {
        switch sequence {
            case KpiSequence.first:
                self.backgroundCircleImage.image = #imageLiteral(resourceName: "first_kpi_icon_active")
                self.kpiValue.textColor = ResColor.colorTextFirstKpi
            case KpiSequence.second:
                self.backgroundCircleImage.image = #imageLiteral(resourceName: "second_kpi_icon_active")
                self.kpiValue.textColor = ResColor.colorTextSecondtKpi
            case KpiSequence.third:
                self.backgroundCircleImage.image = #imageLiteral(resourceName: "third_kpi_icon_active")
                self.kpiValue.textColor = ResColor.colorTextThirdKpi
        }
        
    }
    
}
