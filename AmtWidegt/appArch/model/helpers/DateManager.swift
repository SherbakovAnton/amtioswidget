//
//  DateManager.swift
//  AmtWidegt
//
//  Created by Mac on 2/20/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation

public func getInterval(_ period : Period) -> (String, String) {
    let date = Date()
    let formatter = DateFormatter()
    let dateFormat = "yyyy-MM-dd HH:mm ZZZ", utc = "UTC"
    
    formatter.dateFormat = dateFormat
    formatter.timeZone = NSTimeZone(abbreviation: utc)! as TimeZone
    let currentUtc = formatter.string(from: date)
    
    var dateComponent = DateComponents()
    
    switch period {
    case .day:
        dateComponent.day = -1
    case .week:
        dateComponent.day = -7
    case .month:
        dateComponent.month = -1
    case .year:
        dateComponent.year = -1
    }
    
    let pastUtc = formatter.string(from:Calendar.current.date(byAdding: dateComponent, to: date)!)
    
    
    return (from : currentUtc, to : pastUtc)
}

public enum Period {
    case day
    case week
    case month
    case year
}
