//
//  Layer.swift
//  AmtWidegt
//
//  Created by Mac on 1/31/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import RxSwift
import RealmSwift

private let userLayer = UserLayer()
public class UserLayer {
    
    var user : User? = nil
    let observableUser = PublishSubject<User?>()
    let observableUserKpiList = PublishSubject<Array<KpiIndicator>>()
    
    class var instanse : UserLayer {
        return userLayer
    }
    
    //******************************** METHODS *******************************
    
    func getUser() -> User? {
        print("user getUser - \(Repository.getUser())")
        return Repository.getUser() }
    
    func getUserId() -> String? {
        return Repository.getUser()?.id
    }
    
    func saveUser(user : User) {
        //log.debug("saveUser \(user)")
        print("user - saveUser \(user)")
        Repository.saveUser(user : user) }
    
    func getKpiIndicators() -> Array<KpiIndicator>? {
        
        return [KpiIndicator().apply {
            $0.index = 1;
            $0.kpi = Kpi().apply {
                $0.id = 1
                $0.name = "Damage Rate"
            }
            }, KpiIndicator().apply {
                $0.index = 2;
                $0.kpi = Kpi().apply {
                    $0.id = 2
                    $0.name = "All ordered vehicles"
                }
            }, KpiIndicator().apply {
                $0.index = 3;
                $0.kpi = Kpi().apply {
                    $0.id = 4
                    $0.name = "Terminal osrded vehicles"
                }
            }]
        //return user?.kpiInicatorList
    }
    
    func getBrand() -> String {
        return "Renault"
    }
    
    func onChangeUserKpi(kpiList : Array<KpiIndicator>) { observableUserKpiList.onNext(kpiList) }
    
    func isUserLogIn(callback : PublishSubject<Bool>) {
        let isLogged = getUser()?.generalToken() == nil ? false : true
        callback.onNext(isLogged)
    }
    
    func isUserLogIn() -> Bool {
       return getUser()?.generalToken() == nil ? false : true
    }
    
    func onLogin(user : User) { observableUser.onNext(user) }
    
    func logout(callback : PublishSubject<Bool>) {
        do {
            let realm = try! Realm()
            try! realm.write {
                realm.deleteAll()
            }
            callback.onNext(true)
        } catch {
            callback.onNext(false)
        }
    }
    func onLogout() { observableUser.onNext(nil) }
}

public class Repository {
    
    /** REALM INSTANCE */
    //static let realm = try! Realm()
    
    /** ABSTARCT REALM WRITE FUNCTION */
    static let write : (Object) -> Void = { realmObj in
        let realm = try! Realm()
        try! realm.write { realm.add(realmObj, update : true) } }
    
    /** ABSTARCT REALM WRITE FUNCTION */
    static func first<E : Object>(type : E.Type) -> E? {
        let realm = try! Realm()
        let entity = realm.objects(type).first
        return entity
        
    }
    
    
    
    /** SAVE USER */
    static func saveUser(user : User) { write(user) }
    
    /** GET USER */
    static func getUser() -> User? { return first(type: User.self) }
    
    
    
    
    
    
    
    
    
    
}

protocol RealmExtension {}
extension RealmExtension where Self : Object {
    
    func updateEntity(_ block : (Self) -> Void ) {
        let realm = try! Realm()
        try! realm.write {
            block(self)
        }
    }
}

extension Object : RealmExtension { }
