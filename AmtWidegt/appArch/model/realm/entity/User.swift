
import Foundation
import ObjectMapper
import RealmSwift

class User : Object, BaseRealmEntity {
    
    @objc public dynamic var id : String = "0"
    @objc dynamic var login : String? = nil
    @objc dynamic var password : String? = nil
    //@objc dynamic var companyId : String? = nil
    @objc dynamic var primaryToken : String? = nil
    @objc dynamic var secondaryToken : String? = nil
    
    @objc dynamic var company : Company? = nil
    @objc dynamic var brand : Brand? = nil
    @objc dynamic var period : String? = nil
    
    @objc dynamic var firstKpi : Kpi? = nil
    @objc dynamic var secondKpi : Kpi? = nil
    @objc dynamic var thirdKpi : Kpi? = nil
    
    //var kpiInicatorList : List<KpiIndicator>? = List<KpiIndicator>()
    
    required convenience init?(map: Map) { self.init() }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func generalToken() -> String? {
        if let primaryToken_ = primaryToken, let secondaryToken_ = secondaryToken {
            return "\(primaryToken_)@\(secondaryToken_)"
        }
        return nil
    }
    
    public func mapping(map: Map) {
        //id          <- map["Id"]
        login       <- map["Login"]
        password    <- map["Password"]
        //companyId   <- map["CompanyId"]
        //token       <- map["Token"]
    }
    
    /*public static func addKpiIndicator(indicator : KpiIndicator, toList : List<KpiIndicator>) -> List<KpiIndicator> {
        if (toList.contains(indicator)) {
            let elementToDelete = toList.filter { indicator.index.isEqual($0.index) }.first
            let elementIndex : Int? = elementToDelete != nil ? toList.index(of: elementToDelete!) : nil
            if let i = elementIndex { toList.remove(at: i) }
        }
        toList.append(indicator)
        return toList
    }*/
    
    public func toString() -> String {
        return "User [login ]"
    }
}
