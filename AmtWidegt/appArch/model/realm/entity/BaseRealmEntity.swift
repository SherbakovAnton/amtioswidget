import Foundation
import ObjectMapper

public protocol BaseRealmEntity : Mappable {
    
    func toString() -> String
    
}
