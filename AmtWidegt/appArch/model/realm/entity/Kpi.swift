//
//  Kpi.swift
//  AmtWidegt
//
//  Created by Mac on 1/25/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Kpi : Object, Mappable {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var name : String? = nil
    // ignored Realm property
    var value : Double? = nil
    
    required convenience init?(map: Map) { self.init() }
    
    public func mapping(map: Map) {
        id          <- map["Id"]
        name        <- map["Name"]
        value       <- map["Value"]
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
