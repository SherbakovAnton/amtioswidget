//
//  Brand.swift
//  AmtWidegt
//
//  Created by Mac on 1/25/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Brand : Object, BaseRealmEntity {
    
    @objc dynamic var id : String? = nil
    @objc dynamic var name : String? = nil
    
    required convenience init?(map: Map) { self.init() }
    
    public func mapping(map: Map) {
        id          <- map["Id"]
        name        <- map["Name"]
    }
    
    public func toString() -> String {
        return "Company [login ]"
    }
    
}
