//
//  KpiIndicator.swift
//  AmtWidegt
//
//  Created by Mac on 2/1/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import RealmSwift

class KpiIndicator : Object {
    
    @objc dynamic var index = -1
    @objc dynamic var kpi : Kpi? = nil
    
    override static func primaryKey() -> String? {
        return "index"
    }
    
}
