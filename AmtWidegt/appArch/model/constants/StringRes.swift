//
//  StringRes.swift
//  AmtWidegt
//
//  Created by Admin on 13.02.2018.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation

class StringRes {
    
    static let loginIsNotValid = "Login is not valid or empty"
    static let passwordIsNotValid = "Password is not valid or empty"
}
