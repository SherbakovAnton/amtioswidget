//
//  Colors.swift
//  AmtWidegt
//
//  Created by Mac on 2/3/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit

enum ResColor {
    
    static let turquoise    = UIColor(hex: 0x00d3da) // бирюзовый
    static let acidGreen    = UIColor(hex: 0x88f200)
    static let crimson      = UIColor(hex: 0xc6078ea)
    
    static let bottomColor = UIColor(hex: 0x38a3ea)
    
    static let textInputBackground = UIColor(hex: 0x353c4b)
    
    
    /** gradient background */
    static let gradientFrom = UIColor(hex: 0x313949)
    static let gradientTo   = UIColor(hex: 0x171923)
    
    /** gradient in list */
    static let listGradientFrom = UIColor(hex: 0x49bbc5)
    static let listGradientTo  = UIColor(hex: 0x43398a)
    
    /** Text colors */
    static let colorTextFirstKpi    = UIColor(hex: 0xea4c2b)
    static let colorTextSecondtKpi  = UIColor(hex: 0x3ab2b8)
    static let colorTextThirdKpi    = UIColor(hex: 0xfe8993)
    
    /** Text colors */
    static let cellBackground    = UIColor(hex: 0x363c4a)
    
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF
        )
    }
}
