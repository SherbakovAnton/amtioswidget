//
//  WebClient.swift
//  AmtWidegt
//
//  Created by Mac on 1/24/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import AlamofireObjectMapper

class WebClient {
    
    let log = "Alamofire REST"
    
    private init() {}
    
    static let webClientInstance = WebClient()
    
    func getPrimaryToken(_ parameters : Parameters, _ callback : PublishSubject<String?> ) {

        let urlString = "http://amt2.estafeta.org/api/widget/login"
        Alamofire
            .request(urlString, method: .post, parameters: parameters, encoding: URLEncoding())
            .responseString(encoding: String.Encoding.utf8) {
                if let data = $0.data,
                    let utf8Text = String(data: data, encoding: .utf8),
                    let tokenNotNull : Bool?  = (utf8Text != "null") ? true : nil,
                    let isSucces : Bool = $0.result.isSuccess {
                        let cleanToken = utf8Text.replacingOccurrences(of: "\"", with: "")
                        callback.onNext(cleanToken)
                } else {
                    callback.onNext(nil)
                }
                if $0.result.isFailure { callback.onNext(nil) }
        }
    }
    
    func getCompanies(primaryToken : String, _ callback : PublishSubject<Array<Company>> ) {
        
        
        let urlString = "http://amt2.estafeta.org/api/widget/getCompanies"
        
        let url: URL = URL (string : urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("Token \(primaryToken.toBase64())",
        forHTTPHeaderField: "Authorization")
        
        
        Alamofire
            .request(request)
            .responseArray { (response : DataResponse<[Company]>) in
                if let responseEntiites : Array<Company> = response.result.value {
                    callback.onNext(responseEntiites)
                }
            }
    }
    
    func getBrands(generalToken : String, _ callback : PublishSubject<Array<Brand>> ) {
        
        let urlString = "http://amt2.estafeta.org/api/widget/getBrands"
        
        let url: URL = URL (string : urlString)!
        var request = URLRequest(url: url)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("Token \(generalToken.toBase64())", forHTTPHeaderField: "Authorization")
        
        Alamofire
            .request(request)
            .responseArray { (response : DataResponse<[Brand]>) in
                if let responseEntiites : Array<Brand> = response.result.value {
                    callback.onNext(responseEntiites)
                }
            }
    }
    
    func getKpiList(generalToken : String, _ callback : PublishSubject<Array<Kpi>> ) {
        Alamofire
            .request(generateAuthPostRequest(url : apiTestGetKpiList, token : generalToken))
            .responseArray { (response : DataResponse<[Kpi]>) in
                if let responseEntiites = response.result.value { callback.onNext(responseEntiites) }
        }
    }
    
    func getKpiValue(_ body : GetKpiRequest, _ callback : PublishSubject<Kpi>) {
        let urlString = "http://amt2.estafeta.org/api/widget/GetKeyPerformanceIndicatorValues"

        let jsonStringEntity = body.toJSONString(prettyPrint: true)
        let dataEntity: Data = jsonStringEntity!.data(using: .utf8)!
        
        
        let url: URL = URL (string : urlString)!
        var request = URLRequest(url: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("Token ZjhhYTU0ZDU0Y2YyNGJjMTkxZDhiZDQ5MDViMmFkZGZiZDkyYWQ0MDY4YjM0MzIyOTcxYjU2ZjdmZTA5NGQ1YUBmOThlN2U2Yi1lNjQ4LTRiYWUtODJmOC1jMDI1Mzc5ZGQ5ZmM=", forHTTPHeaderField: "Authorization")
        request.httpBody = dataEntity
        
        Alamofire
            .request(request)
            .responseArray { (response : DataResponse<[Kpi]>) in
                if let responseEntiites : Array<Kpi> = response.result.value {
                    if let letKpi = responseEntiites.first {
                        callback.onNext(letKpi)
                    }
                }
            }
    }
}

private let apiTestGetKpiList = "http://amt2.estafeta.org/api/widget/getKeyPerformanceIndicators"

private func generateAuthPostRequest(url : String, token : String) -> URLRequest {
    var request = URLRequest(url: URL(string : url)!)
    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
    request.httpMethod = HTTPMethod.post.rawValue
    request.setValue("Token \(token.toBase64())", forHTTPHeaderField: "Authorization")
    return request
}
