//
//  GetKpiResponse.swift
//  AmtWidegt
//
//  Created by Mac on 1/30/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation


import ObjectMapper

class GetKpiResponse : Mappable {
    
    var id : Int?
    var value : Int?
    
    required convenience init?(map : Map) { self.init() }
    
    public func mapping(map : Map) {
        id      <- map["Id"]
        value   <- map["Value"]
    }
}
