//
//  GetKpiRequest.swift
//  AmtWidegt
//
//  Created by Mac on 1/29/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import ObjectMapper

class GetKpiRequest : NSObject, Mappable {
    
    var keyPerformanceIndicatorIds : Array<String>?
    var brandId : String?
    var fromDate : String?
    var toDate : String?
    
    required convenience init?(map: Map) { self.init() }
    
    public func mapping(map: Map) {
        keyPerformanceIndicatorIds      <- map["KeyPerformanceIndicatorIds"]
        brandId                         <- map["BrandId"]
        fromDate                        <- map["FromDate"]
        toDate                          <- map["ToDate"]
    }
}
