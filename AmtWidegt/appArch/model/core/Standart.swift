//
//  Standart.swift
//  AmtWidegt
//
//  Created by Mac on 1/24/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit

public protocol Standart {}
public protocol StandartOptional {}
extension Standart where Self : Any {
    
    public func apply(_ block: (Self) throws -> Void) rethrows -> Self {
        try block(self)
        return self
    }
    
    public func also(_ block: (Self) throws -> Void) rethrows {
        try block(self)
    }
    
}

extension Bool : Standart {}
extension NSObject : Standart {}
extension UIViewController : Standart {}
extension URLRequest : Standart {}




