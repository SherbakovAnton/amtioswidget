//
//  DropperExtensions.swift
//  AmtWidegt
//
//  Created by Mac on 1/24/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import Dropper

extension Dropper {
    
    func touchDropper(_ button : ButtonView) {
        if (self.status == .hidden) {
            self.theme = Dropper.Themes.black(nil)
            self.width = CGFloat(button.frame.size.width)
            self.cornerRadius = 3
            self.delegate = button 
            self.showWithAnimation(0.3, options: Dropper.Alignment.center, button: button)
        } else {
            self.hideWithAnimation(0.3)
        }
    }
    
    func setItems(companies : Array<Company>) -> Self {
        var companyNameArray = Array<String>()
        companies.forEach {
            if let companyName = $0.name {
                companyNameArray.append(companyName)
            }
        }
        self.items = companyNameArray
        return self
    }
    
    func setItems(brands : Array<Brand>) -> Self {
        var companyNameArray = Array<String>()
        brands.forEach {
            if let brandName = $0.name {
                companyNameArray.append(brandName)
            }
        }
        self.items = companyNameArray
        return self
    }
    
    func setItems(items : Array<String>) -> Self  {
        self.items = items
        return self
    }
}
