//
//  StringExt.swift
//  AmtWidegt
//
//  Created by Mac on 2/15/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}
