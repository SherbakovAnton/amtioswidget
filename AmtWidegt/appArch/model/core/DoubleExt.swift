//
//  DoubleExt.swift
//  AmtWidegt
//
//  Created by Admin on 12.02.2018.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import Foundation

extension Double {

    func rounded(toPlaces places : Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func toPercentStringFormat(toPlaces places : Int = 2) -> String {
        return "\(Double(self).rounded(toPlaces: places) * 100)%"
    }
    
    func detectCoeff() -> String {
        return self >= 1 || self == 0  ? String(describing : Int(self)) : self.toPercentStringFormat()
    }
}
