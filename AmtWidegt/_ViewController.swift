//
//  ViewController.swift
//  AmtWidegt
//
//  Created by Mac on 1/20/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import Alamofire
import Dropper

class _ViewController : UIViewController {
    
    private lazy var finishLogin : FinishLoginViewController    = FinishLoginViewController.initialize()
    private lazy var firstLogin  : FirstLoginViewController     = FirstLoginViewController.initialize()
    private lazy var mainController  : MainViewController       = MainViewController.initialize()
    
    private lazy var viewModel = FirstLoginViewModel.instance
    private lazy var finishViewModel = FinishLoginViewModel.instance
    
    //**************************************************** METHODS ****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.searchText.subscribe( { _ in self.firstLoginIsSuccessfull() } )
        finishViewModel.loginSuccessfullObservable.subscribe( { _ in self.loadMainController() } )
        add(firstLogin)
    }
    
    func firstLoginIsSuccessfull() {
        add(finishLogin)
    }
    
    func loadMainController() {
        add(mainController)
    }
    
}

