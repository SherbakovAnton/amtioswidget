//
//  ViewController.swift
//  AmtWidegt
//
//  Created by Mac on 1/20/18.
//  Copyright © 2018 Estafeta. All rights reserved.
//

import UIKit
import Alamofire
import RxSwift
import XCGLogger

class ViewController : UIViewController {
    
    private lazy var finishLogin : FinishLoginViewController    = FinishLoginViewController.instantiate()
    private lazy var firstLogin  : FirstLoginViewController     = FirstLoginViewController.instantiate()
    private lazy var mainController  : MainViewController       = MainViewController.instantiate()
    
    private lazy var testController  : ViewController       = UIStoryboard(name: "Main", bundle: Bundle.main)
        .instantiateViewController(withIdentifier: "testController") as! ViewController
    
    private lazy var viewModel = FirstLoginViewModel.instance
    private lazy var finishViewModel = FinishLoginViewModel.instance
    
    private let bag = DisposeBag()
    
    //**************************************************** METHODS ****************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.primaryTokenObservble
            .subscribe(onNext : {
                if let primaryToken = $0 {
                    self.firstLoginIsSuccessfull()
                    let nvc = UIStoryboard(name: "Main", bundle: Bundle.main)
                        .instantiateViewController(withIdentifier: "finishLogin")
                    
                    self.present(nvc, animated: true)
                }
                
                } )
            .disposed(by: bag)
        
        /*finishViewModel.loginSuccessfullObservable
            .subscribe( { _ in self.loadMainController() } )
            .disposed(by: bag)*/
        
        //self.present(firstLogin, animated: true, completion: nil)
        
        add(firstLogin)
    }
    
    func firstLoginIsSuccessfull() {
        log.debug("firstLoginIsSuccessfull")
        remove(firstLogin)
        add(finishLogin)
    }
    
    func loadMainController() {
        log.debug("loadMainController")
        let nvc = UIStoryboard(name: "Main", bundle: Bundle.main)
            .instantiateViewController(withIdentifier: "mainvc") as! UINavigationController
        
        self.present(nvc, animated: true)
    }
    
}

